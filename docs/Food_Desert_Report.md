![header image](https://gitlab.com/our-sci/bionutrient-institute/bi-docs/-/raw/master/docs/img/BI.desert.jpg)

## Introduction
The Food Desert Experiment is part of the larger Bionutrient Institute (BI), formerly the Real Food Campaign, with a mission to better understand nutrient density in our food system. The goal of this experiment is to contribute to the understanding of variation in nutrient density in cities across the U.S. by comparing produce purchased in or near food deserts and produce purchased outside of these areas. We want to know if systemic discrepancies exist in our food system. Where does higher quality, more nutritious produce go and who has access to it? Do economic and geographic factors limit access to more nutrient dense food?
For the purposes of this experiment, a food desert is any area within a city where people have to travel one mile or more to reach a grocery store, as defined by the USDA. The cities surveyed were: Detroit, Newark, Chicago, Oakland, Kansas City, Philadelphia, and Portland. 

## Methods
The USDA’s [Food Access Research Atlas](https://www.ers.usda.gov/data-products/food-access-research-atlas/go-to-the-atlas/) was used to determine the locations and boundaries of food deserts in each city. Ten stores were randomly selected in each city; five within or near a food desert and five in non-food desert areas. These stores were cross-checked with other criteria including the average price of groceries at the store and local knowledge of the city to get a better representation of the food that was actually accessible to food desert communities. If a high-price health food store was selected because it was near a food desert that would not pass the criteria and a new store would be randomly selected.

In each city a Citizen Science partner volunteered to collect samples from the selected stores. At each store volunteers completed a [survey](https://app.surveystack.io/surveys/5ee28eae4f38f80001492008) noting what type of produce was available, purchased five samples (grapes, beets, potatoes, peppers, and zucchini) if they were available, and logged any marketing of the sample (i.e. organic, hydroponic, local, etc.). These crops were chosen because they are widely available and were expected to ship fairly well. Shipping samples to the BI lab can take a few days which can cause crops, especially leafy greens, to spoil in transit. The goal with choosing these specific crops was to avoid that issue as much as possible to have an adequate number of samples to test and compare. Partners also noted the type of store from the following options: national chain - high cost, national chain - low cost, regional chain - high cost, regional chain - low cost, neighborhood supermarket, and convenience store. This information was entered into the survey and the samples were then sent for testing at the BI lab. Table 1 shows the sourcing information for all of the samples included in this experiment.

![Table 1](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Food%20Desert%20Report/Table%201.JPG)
**Table 1.** Distribution of samples by city

Once samples arrived in the lab they were tested for the analytes described in Table 2. Detailed documentation of all lab processes used for crop and soil samples in 2020 can be found here: [Bionutrient Institute - Lab Processes](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/intake/). 

![Table 2](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Food%20Desert%20Report/Table%202.JPG)
**Table 2.** Summary of measurement for all produce samples.

## Results
The lab received 268 samples from all of the cities. Not all of those samples were tested because some had spoiled in transit and others were too small to test. Additionally, in 2020, the BI lab observed that shipping time for samples increased compared to previous years. We believe that the COVID-19 pandemic was at least partially responsible for longer shipping times, which resulted in an increase in spoiled samples. The results below are based on testing for the 215 samples that were good enough to process. 

The test results for antioxidants, polyphenols, brix, and minerals (Mg, P, S, K, Ca, Fe, Zn) of samples from every city were split into categories to be analyzed with statistical testing. In addition to the individual minerals and nutrients, we aggregated all of the measured parameters into the Bionutrient Quality Index (BQI) to better identify trends in the data. For a more detailed definition of BQI see Appendix A.

First, we compared  the food-desert and non-food desert areas by crop type and nutrient using Analysis of Variance (Table 3). There were only a few minerals with significant p-values; S for beets and P for zucchini. There are also significant differences for zucchini BQI and beet polyphenols. Overall this data doesn’t demonstrate a difference in food quality in food desert areas vs. non-food desert areas.

![Table 3](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Food%20Desert%20Report/Table%203.JPG)
**Table 3.** Results of anova testing of analytes for crops group by crop type and food desert or non-food desert area.

Next, the data was grouped by store type to try to understand what other factors may be affecting food quality beyond food desert status. Based on the p-values from the anova tests there were no significant differences in brix or antioxidant values across the different store types and food desert/non-food desert areas (Table 4). Beets, and potatoes do show differences in polyphenol values that are not likely due to chance (Fig. 1). For minerals there were significant differences in calcium and potassium values in beets across store types. There were no significant differences in BQI for any crops. With the exception of beets, there is little evidence that store type has an effect on nutrient density for the five crops analyzed. 

![Table 4](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Food%20Desert%20Report/Table%204.JPG)
**Table 4.** Results of anova testing for crops grouped by crop and store type with significant p-values highlighted.

![Figure 1](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Food%20Desert%20Report/Figure%201.png)
**Figure 1.** Box and whisker plots of polyphenols with significant p-values.

The results were also tested across cities for significance (see figure 2). As with the other groupings there is a lot of variation. However, unlike the previous comparisons, there were many significant differences in nutrient and mineral content between cities (see table 5). 

![Figure 2](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Food%20Desert%20Report/Figure%202.png)
**Figure 2.** Polyphenols and antioxidants by city.

![Table 5](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Food%20Desert%20Report/Table%205.JPG)
**Table 5.** Results of anova testing for crops grouped by crop type and city with significant p-values highlighted. Note that Newark is not included in the grape analysis because there are not enough samples to compare.

Tukey-Kramer testing shows that Chicago is pretty consistently in significant brix comparisons, but the average is both higher (pepper & zucchini) and lower (grape) than other cities. Portland is in multiple significant pairings for antioxidants, always performing better than other cities (see figure 2). For polyphenols, Detroit performed significantly better than Chicago and Portland for both beets and potatoes (see figure 3). Overall there is not one city that is consistently performing better than the others. This data reinforces the large variation that exists in our food system.

## Discussion

When comparing the produce based on store type, food desert area, and non-food desert area, there wasn’t any consistent pattern in terms of which groups performed best, although some minerals and nutrients showed significant differences. These results challenge an assumption that many would make, that produce available near a food-desert or at a lower cost grocery store, would be less nutrient dense than produce available at a higher-cost store.Instead, this data is demonstrating that there is significant variation in our food system, and assumptions about a link between the cost of an item of produce and its quality may not be accurate. 

While store quality may not have affected nutrient density, which city the sample came from had quite the impact. There were many significant differences across analytes for many of the crops, but it is difficult to find a pattern showing which cities performed best. It is worth noting that the sampling was conducted in these cities at different times of year. It’s possible that crops were in season during the sampling in one city and therefore didn’t have to travel as far to get to the grocery store and were of higher quality. There were also some cities that have a very small sample size represented in the graphs above. For example, there were only three peppers tested for polyphenols from Oakland, Newark, and Portland. Small sample sizes could affect the results of anova testing. 

As mentioned in the intro we used the USDA definition of a food desert and their food desert atlas to design this experiment. The definition of a food desert is nuanced and by reducing it down to a geographic definition we may be missing some information, such as income, that would also influence where a person shops and what foods they have access to.



## Appendix A. Definitions
![Box 1](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Grains%20Report/box%201.png)
Calculation 1 is used throughout this report.