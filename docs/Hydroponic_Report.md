![header image](https://gitlab.com/our-sci/bionutrient-institute/bi-docs/-/raw/master/docs/img/BI.tomatoes.jpg)

## Introduction
This experiment is a partnership between [Next7](https://www.next7.org/) and the [Bionutrient Institute](https://www.bionutrientinstitute.org/) (BI), formerly known as the Real Food Campaign. The goal of the study is to compare the nutrient density of organic hydroponic and soil grown tomatoes in an effort to contribute to and expand the connections the BI has made between crop management practices and produce quality. 

## Methods
### Sample and Data Collection
For this experiment a total of 48 organic tomato samples were analyzed. These samples came from grocery stores and farmers markets near Boulder, Colorado. Of those samples 14 were hydroponically grown and 34 were soil grown. In addition there were four varieties analyzed: black krim, roma, golden, and san marzano.

| **Variety** | **# Soil Grown** | **Hydroponic** |
|-----------|---------------|------------|
| Black Krim | 6 | 1 |
| Roma | 15 | 13 |
| Golden | 7 | 0 |
| San Marazano | 6 | 0 |  

**Table 1.** Breakdown of tomato variety and management practice.

Next7 filled out surveys for each sample sent to the lab; providing information such as tomato variety, the store/market it was purchased from, and any information on the growing practices. This process also provided a way to track the sample from purchase to the lab.

### Lab Methods
Once samples arrived in the lab they were tested for the analytes described in Table 2. Detailed documentation of all lab processes used for crop and soil samples in 2020 can be found here: [Bionutrient Institute - Lab Processes](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/intake/).

![Table 2](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/hydroponic%20report/Table%202.JPG)

### Statistical Analysis
roma tomatoes and hydroponic roma tomatoes to eliminate possible variation due to variety and second a test controlling for variety and no other factors. There are not enough samples of each variety to compare hydroponic and soil grown. The values we will focus on are antioxidants, polyphenols, and brix values as well as data for 7 key minerals including magnesium, phosphorus, sulfur, potassium, calcium, iron, and zinc. The Bionutrient Quality Index will be calculated using the results of the lab tests (see Appendix B for BQI definition and calculation).

## Results
Previous BI reports have demonstrated a large amount of variation in nutrient density. The BI has identified climate region, crop color, variety, management practices and soil properties as sources of variation in nutrients. 

In this case all of the samples were purchased in the same climate region. However, there could be significant climate variations between soil grown and hydroponic samples as hydroponic samples are grown in highly controlled environments and soil grown tomatoes are more exposed and susceptible to changes in weather.

After lab data was received, t-tests were conducted for antioxidants, polyphenols, brix, minerals and BQI to determine if the difference in means of the soil grown roma tomatoes and the hydroponic roma tomatoes is likely due to chance or not. Antioxidants and multiple minerals showed significant differences between the soil grown and hydroponic roma tomatoes. 

The difference in averages of antioxidants is statistically significant for a significance level of 0.05 (see Appendix A table A1). This means that we can be 95% confident that this difference is not random or due to chance. In this experiment the antioxidants of soil grown roma tomatoes are less than the antioxidants of hydroponic roma tomatoes  (see Appendix A figure A1). 

However, hydroponic tomatoes had between 6% and 60% lower average mineral content compared to soil grown tomatoes with the reduction in P, S, K, and Zn content being statistically significant (see figure 1 ).

![figure 1](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/hydroponic%20report/figure%201.png)  
<em>Figure 1. Percent change in hydroponic from soil grown roma tomatoes</em>

Both growing methods have large standard deviations demonstrating the variation in antioxidants even within management practices.

The difference in averages of a few minerals including, Zn, K, S, and P as well as BQI were also statistically significant with the soil grown mean being higher than the hydroponic mean in each of these categories. 

When the dataset is grouped by variety the difference in averages of P, Ca, Fe, Zn, and antioxidants are all statistically significant. The sample sizes were small, but this data reinforces the idea that there is considerable variation in our food system, even between varieties of one crop.

## Discussion
This data seems to demonstrate that soil-grown tomatoes have higher mineral content than hydroponically-grown tomatoes. Soil grown tomatoes are going to depend on management of the microbial community in the soil to solubilize minerals for plant uptake whereas hydroponic crops will require direct input of nutrient solutions. The management of each of these systems will likely have a large impact on the mineral content of the crops they produce. 

However, it is important to note a few things. The comparison between hydroponic and soil grown tomatoes was only done on roma tomatoes that were collected from one geographical area and there was a fairly small sample size. BQI is used as an indicator because it aggregates a set of complex numbers to identify trends and here we are seeing a trend that is worth further exploration. Continuing to expand this data set to include different varieties as well as different crops could lead to the emergence of other significant patterns and provide further data to support this study. Additionally, the lab analyses also only measured a few nutrients and minerals. Further testing would give a more complete accounting of the nutrient density.

Finally it is also important to consider other factors in these two farming systems, one of which is environmental impact. Hydroponic systems often require more energy to grow because of the lighting systems they require. One [article](https://www.mdpi.com/1660-4601/12/6/6879/htm) from the International Journal of Environmental Research and Public Health found that about 80 times more energy was used to grow lettuce in Arizona hydroponically as compared to conventional soil-grown methods. However, that same article notes that there was higher yield with hydroponics and it used less water. Another [article](https://www.sciencedirect.com/science/article/pii/S221282711730820X) showed that in a Life Cycle Assessment an urban  vertical  hydroponic system outperformed conventional soil grown lettuce in marine eutrophication (see appendix B) and land occupation, but underperformed in the categories of climate change, freshwater eutrophication, freshwater ecotoxicity, and fossil depletion. Electricity use was a major factor in the climate change category. Hydroponic systems can provide food year-round and can be utilized especially in urban areas where there is not a lot of land to grow food. This could help reduce the carbon footprint of transporting food into cities, but the environmental impacts of using that amount of energy should be considered as well as the source of electricity.



## Appendix A.
|   | **Mean - soil grown** | **Mean - hydroponic** | **p-value** |
|------|---------|------------|-----------|
| **Antioxidants** | 2789.80 | 4097.36 | <ins>0.001</ins> |
| **Polyphenol** | 97.89 | 79.98 | 0.075 |
| **Brix** | 3.29 | 3.43 | 0.25 |
| **Mg** | 9.04 | 7.77 | 0.133 |
| **P** | 25.51 | 13.55 | <ins>0.000</ins> |
| **S** | 11.71 | 7.51 | <ins>0.000</ins> |
| **K** | 198.26 | 185.02 | <ins>0.027</ins> |
| **Ca** | 7.64 | 6.64 | 0.338 |
| **Fe** | 0.40 | 0.37 | 0.073 |
| **Zn** | 0.17 | 0.13 | <ins>0.001</ins> |
| **BQI** | 0.41 | 0.33 | <ins>0.003</ins> |  

**Table A1.** Results of t-test comparing soil grown roma tomatoes to hydroponic roma tomatoes with significant p-values highlighted.

![antioxidants](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/hydroponic%20report/antioxidants.png)  
**Figure A1.** Box plot of antioxidant values for each variety and management of tomato tested.

![polyphenols](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/hydroponic%20report/polyphenols.png)  
**Figure A2.** Box plot of polyphenol values for each variety and management of tomato tested.

The average polyphenols (ppm) of soil grown roma tomatoes is 97.9 with a standard deviation of 15.8. The average polyphenols (ppm) of hydroponically grown roma tomatoes is 80.0 with a standard deviation of 29.2. There was significant variation in polyphenol content with a range of 48 to 153 ppm.

![brix](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/hydroponic%20report/brix.png)  
**Figure A3.** Box plot of brix values for each variety and management of tomato tested.

San Marzano tomatoes were not included on the graph above because out of the 6 soil grown san marzano tomatoes tested in the lab only 2 produced enough juice to measure with the brix meter. Therefore there is not enough data for that variety to show on this graph. The average brix value of soil grown roma tomatoes is 3.3 with a standard deviation of 0.3. The average brix value of hydroponic roma tomatoes is 3.4 with a standard deviation of 0.3. 


## Appendix B. Definitions
![box 1](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Grains%20Report/box%201.png)

Calculation 1 is used throughout this report.

Eutrophication - the process by which a body of water becomes enriched in dissolved nutrients (such as phosphates) that stimulate the growth of aquatic plant life usually resulting in the depletion of dissolved oxygen
