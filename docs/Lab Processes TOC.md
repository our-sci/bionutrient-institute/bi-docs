## BI Lab Processes
- Lab Processes (current): 
  - [Intake](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/intake/)
  - [Full Spec Procedure](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/full_spec/)
  - [Extraction](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/extraction/)
  - [Wet Chemistry Assays](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/wet_chem_assays/)
  - [Moisture Content and XRF-Prep](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/moisture_content/)
  - [Soil Grinding](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/soil_grinding/)
  - [Soil Scanning](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/soil_scanning/)
  - [Soil Respiration](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/soil_respiration/)
  - [Soil pH](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/soil_ph/)
  - [Total Carbon via Loss on Ignition (LOI)](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/loi/)
  - [General glassware/labware cleaning](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/glassware_cleaning/)
  - [Acid Bath for Glass Culture Tubes](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/acid_bath/)
  - [Lab Equipment List](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/lab_equipment/)
  - [New Lab Checklist](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/new_lab_checklist/)

