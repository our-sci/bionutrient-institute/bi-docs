# Blueberry Variety Testing

## Introduction/Methods
Previous research has shown that crop variety can affect nutrient density. In this study researchers tested three different blueberry varieties (Aurora, Elliot, and Bluecrop) to determine if there are significant differences in nutrient density across these varieties.

Samples of three late season varieties were collected from five different farms in southwest Michigan along with soil cores from zero to eight inches. Information about farm practices was also collected.

Once samples arrived in the lab they were tested for the analytes described in Table 1. Detailed documentation of all lab processes used for crop and soil samples in 2020 can be found here: [Bionutrient Institute - Lab Processes.](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/)

![Table 1](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Blueberry%20report/table%201.JPG)

## Results
The samples were grouped by variety and anova tests were run to determine if the differences in Brix, polyphenols, and antioxidants between varieties is likely due to chance or not. The p-values from the anova testing are listed in the table below. 

![Table 2](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Blueberry%20report/Table%20%202.JPG)

**Table 2.** Results of significance testing

Using a significance value of 0.05 it was determined that the differences in brix values between the three varieties is unlikely due to chance or random factors.

![Figure 1](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Blueberry%20report/brix.png)

The minerals Mg, P, S, K, Ca, Fe, and Zn were also analyzed for significant differences between varieties using anova testing along with BQI, a measure to quantify overall quality using the analytes above as well as mineral data. 

![Table 3](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Blueberry%20report/table%203.JPG)

## Discussion
This data supports the claims that variety has an effect on nutrient density. In this case there is not one variety that is clearly more nutrient dense than the others, but there is significant variation.

It is important to note that this data only came from one location where the soils and climate are similar. If a survey across the United States were conducted different patterns may appear. Additionally there are other factors that can affect the nutrient density, an important one being farm management practices. These were not held constant across all five farms although many of them do use similar irrigation and amendments.

## Appendix A
![Table A1](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/Blueberry%20report/table%20A1.JPG)  
**Table A1.** Results of post hoc testing.