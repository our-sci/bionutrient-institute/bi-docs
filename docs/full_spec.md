# Full Spec: NeoSpectra Reflectance

**Safety note:** The light from the NeoSpectra when it is running is very hot. Do not allow your skin to be held in it’s path as it could cause burns.  

### Required Equipment:

- Small Glass Petri Dish
- Top Magnetic Plate with white Teflon disk on the inside
- Middle Plate with housing for glass dish
- NeoSpectra Micro Sensor in housing
- Computer with SpectroMOST software installed

### Running the Full Spec

1. Install the SpectroMOST software by plugging in the NeoSpectra module into the computer using BOTH USB cables (the 2nd cable turns on the 3rd light in the Neospectra module, so that’s important!), waiting 30s for it to turn on, and then finding it as a connected drive on your Windows (only works on Windows :\) computer. There is SpectroMOST software contained on the module which can be installed.

2. Make sure the viewing window is clean. Clean gently with kimwipes that are lightly wetted with water. Ensure the teflon disk is also clean before use.  This can get dirty when put into contact with samples.

3. Open the SpectroMOST software.  Make sure scan time = 20 seconds, and auto-save is turned OFF. 

![fullspec1](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/spectroMost%20software.png) 

4\. ONCE PER DAY (beginning of scanning session)

   a. Warm Up
      - Set measurement mode to ‘cont’, and press ‘Run’ to preheat the bulb.  Preheat for 5 - 10 minutes before starting a set of runs.  

![fullspec2](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/cont%20run.png)

      - Ensure that you hit ‘Stop’ after you have preheated the bulb for 5-10 minutes as it will not stop on its own when in ‘Cont.’ mode.  Set a timer if necessary.

![fullspec3](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/stop%20run.png)

      - Redo this warm up step if it has been more than 1 hour between runs.

   b. Now set measurement mode to ‘single’ for the remainder of normal use.

![fullspec4](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/single%20run.png)

   c. Background Run (running teflon blank): Place the Teflon blank top on the device, face down, centered. Step back from the device, and press the ‘background’ button. This takes a background value from which the other values will be determined. This only needs to be performed once per day, but the software may require you to do it again if there is a long delay between running samples.  

![fullspecBlank](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/full%20spec%20device.png)

   d. Spectralon Standard (running teflon blank three times): Now place the Teflon blank top on the device again, face down, centered. Take 3 measurements, picking it up and placing it back down each time, using the ‘run’ button. Try to place the blank back onto the device in the same spot.  When all three runs are complete, hit ‘Save’ and name the file according to the file naming convention below. This should be performed once per day at the beginning of a set of measurements.

![fullspec5](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/spectrum.png)

5\. FILE NAMES: all file names follow the following convention, using lower case letters:

   a. <sample-type>-<sample-id> (For spectralon standard, please add the data to the file name.  )

   b. sample-type 
 
       i. sp = spectralon standard
       ii. ti = tissue (food, leaf, etc.)
       iii. ju = juice (juiced food sample)
       iv. s = soil 0 - 4cm

   c. Examples of file names

       i. Example of raw tissue scan for sample 2083: ti-11-2083  

       ii. Example for soil scan for sample 11-2502: s-11-2502 

       iii. Example for spectralon standard: sp-mm-dd-yy

   d. Each lab should designate a local folder on the fullspec laptop to save the data to and ensure that the files are saved in the same place each time.

![fullspec6](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/save%20spectra.png)

![fullspec6a](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/full%20specfolder%20selection.png)

***NOTE: Following the naming convention for the file names and saving the files into the correct folder are extremely important. Please double check that no typos have been made before saving the files. 

6\. Preparation for tissue (raw) sample of each type:

   a. **ALL SAMPLES** - Using a lightly moistened paper towel or kimwipe, wipe off the surface of the spectrometer. If you are using the glass juice cup, ensure it is clean between every use. If you are using the Teflon Blank, wipe it down between measurements with a lightly moistened paper towel.

   b. **Leaves (x3) (kale, spinach, lettuce, mustard, mizuna, chard, leak, bok choy )** - Choose 3 leaves (or the max number of leaves you have available). Avoid very large veins, holes, and discolored spots over the measurement window. The leaf should be placed top side (usually dark side) down. Place the Teflon blank top on top of the leaf, as centered and as flat as possible (use the magnets to help center and press down, don’t press down with your finger or hand). Take 3 measurements, each on a different section of the leaf.

![fullspecScan](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/leaf%20scan.png)

   c. **Cherry Tomatoes, grapes, blueberries (x3)** - Choose 3 different pieces of average size. Only conduct the test if the fruit covers the majority of the optical window. This will not be the case for most blueberries as they are likely too small in which case you should skip this step and move on to juice. Do not place the object on the stemmed end or the tip - place it along the side. Place the middle plate on the full spec and the fruit in the center of the optical window. Place the top plate with the teflon disk face up on the fruit and press down firmly to ensure the bottom face fully covers the measurement surface. For very small diameter objects, you may need to make a small cut in the side to let the object and flex enough to be pressed flat. But, try to avoid allowing any juice to drip down onto the surface.  If it does drip, please make sure this is wiped before the next measurement. Take 3 measurements using different tomatoes / grapes each time. Try to center the sample as much as possible.

![fullspec7](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/round%20fruit%20scan%201.png)

![fullspec7a](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/round%20fruit%20scan%202.png)

![fullspec7b](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/round%20fruit%20scan%203.png)

   d. **Roots and tubers (x3)** - Choose a portion will be wide enough to cover the optical window. Place the object, using a surface as flat as possible, and try to center it so that it fully covers the optical window. Take 3 replicates, using one piece per replicate or if there is only one piece, choose different sides of the piece. If it helps to hold the produce, you may use the middle plate to keep it in place.

![fullspec8](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/tuber%20scan%201.png)

![fullspec8a](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/tuber%20scan%202.png)

   e. **Juice (x1)** - Using a 1ml pipettor, measure 1ml of juice into the small glass petri dish (a), ensuring it has wetted the entire bottom surface.  Place the middle plate on the scanner and the small glass petri dish centered in the middle.  Now place the top magnetic plate (c) on top of the glass petri dish.  Now run the sample once.  IF THERE ISN’T ENOUGH JUICE for example from kale or lettuce, select some of the stem and attempt to re-juice.  If you do not have a minimum of 1mL of juice, skip this test. Run sample once.

![fullspec9](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/juice%20scan%201.png)

![fullspec9a](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/covered%20juice%20scan.png)

   f. **Soil (x1)** - Similar to juice. Place the glass petri dish on the scanner with the middle plate. Place at least 5mm thickness of soil in the petri dish, centered in the holder. Cover with the top magnetic plate (c). Run sample once.

7\. ADDITIONAL NOTES

   a. Please keep the equipment clean.  The optical window and the teflon disk particularly need to be wiped down regularly. 
   
   b. Scan Time - evaluated at 2, 5, 10, and 20. There is a ‘ripple’ type noise which is fairly repeatable, but will impact our ability to distinguish critical features in the line. That said, most noise is biological, and getting more measures from a single sample is more important than higher quality individual measures.  

   c. Scan replicates - The juice and soil samples are very repeatable regardless of orientation. As such, a single sample is ok. The physical samples (kale, tomato, etc.) are so variable that replicates are required.

   d. Background - the optimal background (blank) is against the teflon. All samples should use the teflon disk in the top plate (c) as background only.
