# General Labware Cleaning

## Cuvettes
NEVER LEAVE CUVETTES OVERNIGHT WITH REAGENTS OR SAMPLE INSIDE. CLEAN AS SOON AS POSSIBLE AFTER USE.  THIS IS OF PARTICULAR IMPORTANCE WITH JUICE AS IT IS VERY STICKY.

Cuvettes are made of glass, very fragile, and costly with a limited supply in the lab. Handle with great care. Any breakages should be placed in the glass disposal bin. Rinse each cuvette under the tap 3 times, making sure to fully empty the cuvette between each rinse. Rinse three times with the DI water squirt bottle. Lay out large kimwipe on a tray, touch the top of the cuvettes to the kimwipe to help pull water out, wipe the outside of the cuvettes with a kimwipe. then lay out the cuvettes to dry.  The tray can be placed in the food dehydrator to dry faster.

When a deeper clean (on a sunny day) is needed, put 4% H2O2  into a squeeze bottle. Fill each cuvette with H2O2 and place it in a gallon size Ziplock bag with more H2O2 so that the cuvettes are soaking in the bag.  Try to seal the bag with minimal air.  Lay out large Kimwipes on a tray, then lay out the bag of cuvettes so that the bag is flat and all the cuvettes are spaced apart from each other.  Label the tray as “Property of Our Sci”.  Bring the tray to the table outside and let the cuvettes in H2O2 sit in the sun for a few hours.  This can also be placed on the dashboard of a car to get enough sun in a colder climate.  This should remove all blue color from the cuvettes.  When clean, rinse with DI and lay out the cuvettes to dry.  The tray can be placed in the food dehydrator to dry faster.


## General Glassware/Labware Cleaning

1. Empty tubes of all testing solutions into the appropriate waste containers and give a good rinse under the tap.
2. Washing steps
    a. Option #1 (small quantity): Fill a squirt bottle with approximately 1 Tbsp of alconox powdered detergent.  Fill with DI water and gently mix.  This will make a concentrated cleaning solution.  Squirt a small amount of the solution into each item to be cleaned.   
    b. Option #2 (large quantity):  a plastic wash basin can be used to create a cleaning solution using  2 Tbsp/Gal or 10 g/L of alconox powdered detergent to tap water and the glassware cleaned directly in this solution.  

3. Use the appropriate scrub brushes for each item, i.e. use test tube brushes for the tubes
4. Rinse well with tap water after washing with detergent.
5. Triple rinse with DI water. 
6. Drying:
    a. Allow plastic tubes to air dry as a drying oven can turn them yellow.  
    b. Glass cuvettes and culture tubes can be oven dried if necessary.  

7. The most important thing is to do a visual check that each tube has been thoroughly cleaned of all produce matter and then rinsed thoroughly.  
