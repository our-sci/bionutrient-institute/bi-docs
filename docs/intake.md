
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

# Intake

**Table of Contents**

1. [**Notes before Starting**](#notes-before-starting)
2. [**Produce Intake**](#produce-intake)
3. [**Instructions by Produce Type**](#instructions-by-produce-type)
4. [**Grains Intake**](#grains-intake)

## Notes before Starting <a name = "starting"></a>

1. All measurements should be taken using the Bionutrient Institute SurveyStack [software](https://bionutrient.surveystack.io/) . Thorough instructions are included in the software and these instructions are meant as a supplement to that.  

2. Ensure the produce is in good condition and that there is sufficient sample for testing. If a sample is rotten during receiving, please indicate it in the receiving survey, discard it, and nothing else needs to be done. If it was acceptable during receiving but turned rotten before intake, indicate that it's rotten in the intake survey. Ensure the survey is “submitted” so that there is a record of why no testing was done on that sample.

3. Process as much sample as possible and ensure it is mixed as fully as possible to ensure equal distribution of the analyte into the sample. The main idea is to take a representative sample. Hints are given in the instructions about how to accomplish this, but always keep that in mind if you have a sample that might be slightly different than the instructions indicate.  

4. Samples are often affected by oxidation. Once the intake process has begun (i.e. grating) ensure you are working quickly to reduce sample oxidation. Do not allow the sample to sit out for an extended period of time before finishing the intake steps. I.e. don’t go to lunch or on a break until you are finished with an individual sample.

5. Sample manifests: Several sample manifests exist to track movement of samples through the lab process, consisting of wet chem and soil manifests. Each laboratory will handle this in the way that is preferable to their lab.  Please see your lab manager for instruction.

6. The beauty of SurveyStack is that all surveys are centrally stored.  This is a great improvement upon the legacy software that could only be saved on the device the data was recorded on.  As long as you submit the survey, you should be able to access the record on any device that is compatible with SurveyStack.  Always submit the survey.

7. Laboratory testing requires access to each lab's data.  This means that you will need to have an individual login when entering data.  If you have multiple groups assigned to you, always make sure you have designated the correct group when entering data.  This can be done by clicking on your name/email in the upper right hand corner.  The drop down menu will have a list of all groups you are associated with.  Generally, you will want to make sure that you are assigning data based on your specific group (i.e. Chico, Michigan, France).  If you do not specify which group you are working with and it defaults to a higher group, it will make accessing your data more difficult for your group manager and may make it less secure.  Note: Always logout when you are not using the software so that no one uses it under your login data.

### Connecting Devices
The reflectometers are connected to the Our-Sci app through bluetooth.  The balance can be connected through a cable from the balance to the tablet. 

Connecting to reflectometer:

   a. When connecting to any of the devices, sensors, or balances, hitting the measurement button will take you out of SurveyStack and back into the legacy software.  This is where you will control the devices, facilitate their connections, and take measurements.  After the measurement is taken you will have the option to hit the 'done' button and that software will take you back into the survey.


   b. You must connect to the reflectometer through bluetooth. Turn on the reflectometer by pressing the button on the side. Then click on the reflectometer icon in the upper right hand corner in the legacy software.
![connect1](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/connect%20reflectometer.jpg)


   c. If this is the first time you are connecting a reflectometer to this tablet, you will need to pair the device.  Hit ‘Start Search’ and when you find the appropriate device, hit ‘pair’. Enter code 1234. 


   d. You can attempt to hit ‘connect’ but if connection doesn’t happen the system may need to refresh by hitting ‘start search’. Then click on the appropriate reflectometer #. In this example, it is 152. When it is connected, it will give you the option of ‘Disconnect’ or ‘More’.Hit the back button to get back to the scanning screen.  If you have connected the balance and are switching to the reflectometer, you may need to click on ‘disconnect’ before it will connect to the reflectometer. 

![connect2](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/search%20devices.jpg)

   e. Start scan by hitting “Run Measurement”.

![connect3](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/run%20measurement.jpg)

Connecting to Balance:

   a. Some balances have the option to connect to the software. Refer to the balance user manual to apply the electronic connection. If balance connection is possible, ensure the cable is connected during weighing activities and follow the above process for reflectometer connection. To take a measurement, hit “run measurement” in the survey and then hit “print” on the balance.

   b. Surveys will have a connection question prior to any weighing activities in the survey. If the balance can be connected, answer ‘yes’. If it cannot be connected, answer ‘no’ and enter the data manually. **You must answer one way or another or the data you enter may be LOST. Take care. The answer that you select will apply to all balance data points in the survey. If you change the selection after data has been entered, that data may be LOST.**

## Produce Intake

### Required Equipment:

**Intake Software:**
Produce intake data will be entered into the "2021 RFC Intake Survey" in the Bionutrient SurveyStack app. The app provides directions for produce intake. The following data will all be entered or calculated in the survey. The Intake surveys collect the following information:

1. Visual characteristics of each sample
2. Sample weight and density
3. Sample moist weight and sample cup moist weight
4. Reflectance Measurements
5. Juice brix measurements and reflectance scans
6. Up to three samples can be composited into one sample per survey.  However, less than three can be entered based on sample quantity and individual laboratory policy.  

Equipment:

- Knife 
- Cutting Board    		
- Balance (connectivity to tablet is an optional feature)
- Centrifuge tubes with caps (15 mL)   
- Centrifuge tubes with caps (50 mL for grains only)
- Large graduated cylinder (1 L-2 L) 
- Small graduated cylinder (50mL) 
- Large Tongs (optional)
- Reflectometer with narrow cuvette holder
- Reflectometer with masking capability
- Coffee Grinder
- Food Processor or Large Box Grater
- Garlic Press
- Vegetable Peeler
- Powder Shaker
- Narrow Cuvettes
- 1 mL Pipette
- Paper portion cups
- Brix Meter
- Tweezers (optional)
- Ruler
- [Wet Chem Manifest to record samples](https://gitlab.com/our-sci/resources/-/blob/261696bc86eca0e9afe71b494927b0a09d3e389d/resources/wetchemmanifest.pdf)

### Notes about the "2021 RFC Intake Survey"

- The survey should be used through your Chrome browser.  https://bionutrient.surveystack.io/
- Always ensure you are contributing directly to your lab.  In the upper right hand corner of the app, you can click on your email address and it will display your active groups.  Ensure you always have your lab selected or your data will not be accessible properly by your lab manager and will need to be moved later.
- Always use the buttons provided in the survey when going back to a previous screen.  If you use your browser's back button it will take you out of the survey.  You should be able to access the survey again by hitting "My Submissions" in the menu on the left side of the app.  You'll have the option to search for draft or sent or surveys.  A survey that you were just working on should be at the top of the drafts section.
- The survey is designed using relevance statements, so based on your answers to the questions, it should direct you to the correct next question.  For example: if you are testing a leafy green, it will direct you to the leafy length measurements.  If you are testing a round fruit, it will skip length measurements and take you to density.  
- Many of the questions are required fields and are designated with an asterisk *.  A lot of thought went into deciding which questions should be required and if you do not fill in a required question, it will not let you move forward (although there are work arounds, please only use those if absoluteley necessary).  If you feel that a particular question shouldn't be required for some reason, please let Our Sci know the reason and we will change the restriction if necessary.  The main reason why a field is required is because we may need the data in order to collect enough information for the test.  It could also be a requirement in order to make the relevance statements work correctly.  If you did not choose "produce type", for example, it wouldn't know which questions to send you to next.
- As mentioned in the previous section, measurements taken using a balance, reflectometer, or other device will be done through the software sending you back into the legacy software.  Please be careful to follow all prompts and hit the 'done' button when the measurement is completed.  The data will be printed on the screen in the survey, but you will not be able to see the scan once you have left the legacy software.  
- Always log in to your account and log out when you are finished to ensure your work is appropriately attributed to you.

### General Process Notes

<details>

     <summary>
     
     Scanning
     
     </summary>

<h4> #### Scanning Round Fruit: </h4>

For round fruit, use the round fruit attachment with the reflectometer.

![intakeRefl](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/round%20fruit%20scan.png)

The attachment is fitted with magnets that allow it to snap into place.  It should sit flat and snug with a portion of the optical window displayed.  If the attachment doesn't sit on the device correctly, you will not achieve a uniform scan and the data will be no good. If you have any issues with this piece sitting properly, please notify Our Sci.  The cuvette holder has a catch that will allow it to be held out of place while conducting this scan. 

Take a scan from up to three separate pieces, depending on how many you are compositing.  If there is only one sample, take scans from different parts of the sample. Always take a representative sample. For example, if the samples are multicolored, take the three scans from areas that have a variety of colors.  Ensure you cover the optical window as fully as possible when conducting the scan and keep the produce still while conducting each scan. Avoid allowing any light to escape.

Follow the same process with the full spec.  Always try to take scans from the same areas that you scanned with the reflectometer.  You can use the glass dish attachment with the full spec if it allows you to to take a better measurement.  However, it might be best to lay the produce as flat as possible on the optical window without this attachment. Use whichever one gives you the best results depending on the size and shape of the piece. Always try to cover the window as much as possible without moving the sample during scanning.  Remember that the full spec emits a very hot beam of light, do not put your fingers directly over the optical window.  Often times you can use the full spec lid to hold down the produce.

Ensure that you press down firmly on round fruit so that it fully covers that optical window and is less likely to move.  Many fruit pieces are somewhat flexible and can be molded to the optical window with some force.  Avoiding breaking the skin and allowing juice to be released. 

#### Scanning Leafy Greens:

Choose three leaves that are representative of the whole set.  If you receive three clearly defined, separate samples that will be composited into one, make sure you take one leaf from each set.  Place the leaf with the top/dark side down.  Avoid any blemishes, damaged areas, and veins, whenever possible.  Try to make the leaf as flat as possible.  This can be challenging with bumpy leaves, but do your best to flatten it using the cuvette holder as needed.  Ensure the cuvette holder is clamped down well and that the entire optical window is covered.  Repeat the process with the full spec, scanning the same areas that you scanned with the reflectometer. Use the lid to the full spec to hold the sample down flat. Remember that the full spec emits a very hot beam of light, avoid putting your fingers directly over the optical window.  

![intakeleaf](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/green%20leaf%20scan.jpg)

</summary>

</details>

<details>

     <summary>
     
     Density
     
     </summary>


For density you will need the weight and the volume of the water displacement for all samples that you are processing.  If you are processing only one sample this will be easy.  If you are processing up to three samples, you will need to take a weight measurement of all three together and also take the volume of all three together (see butternut for exceptions).  If your balance does not accommodate that much weight, you will have to manually add the weight values together to get a total for all three. Many produce types can fit into a large graduated cylinder, even if you have three pieces.  If your equipment can't accommodate all of the samples together, you will have to use a separate vessel to determine the volume displacement.  For volume displacement, you will record the initial water level before adding the produce and then the final volume after the produce is added.  The difference between the two is the volume of water that was displaced.  It is also important to indicate how many samples you are measuring as this will be involved in the density calculations.  Try to choose three representative samples that will average the values out as accurately as possible.  This will need to be determined by the technician each time. 

There are several ways to take the volume displacement measurement if you cannot use a graduate cylinder and you need to use a large vessel.  As long as you can record the volume accurately, you should do whatever works best for you.  Here are some options, but feel free to use any method that gives accurate results.

1) take a large vessel, add small, known volumes, marking the levels as you go.  You can use that to determine the change in volume.  For example.  Add 100mL and mark the levels as 100mL, add another 100ml and mark as 200mL, etc. 

2) Add a known initial volume to the vessel (enough to cover the produce once added). Add the produce and mark the final volume level.  Remove the produce, and then record how much extra water you need to add in order to fill up to that final volume mark.  Subtract 

3) Put the produce in an empty container.  Add a known volume that covers the produce and mark that spot.  Remove the produce and record how much extra water you need to pour in to get to the initial level and that will be your displaced value.    

You want to ensure that the density volume measurements are taken after the weight measurements in most cases as you do not want to weigh a water logged sample.  If the sample is cut or damaged, it might not be possible to take the volume measurement without contaminating the sample with extra water, this would be the case for a sample such as tomato.  Skip it if you have to.  

When placing produce in the volume vessel, it is important to ensure that it is fully immersed in the water, but you also don't want to cause any additional displacement by pushing down with your hands.  Use a narrow tool such as a metal weighing spatula to ensure the produce is immersed.  

FYI, the density weight measurements are set up in the survey to be manually entered only due to the complexity of taking several measurements affecting the layout of the survey.

DENSITY calculations:   

       p=m/v        

       where: 

       p=density

       m=mass

       v=volume


</details>

<details>

     <summary>
     
     Leaf Measurements
     
     </summary>

Select the three longest leaves from each head or bunch.  Take a measurement with a ruler from the top of the leaf to where the stem meets the leaf.  If necessary, you can attach two rulers together or use a yard stick.  Record the data in cm and take a measurement for up to three samples, depending on how many you have to composite.  If you have one sample, just take one measurement and leave the other two fields blanks.  If you have up to three samples or sample groups, take three measurements.  Only the first measurement field is required, but you should fill in all the data if you have more than one sample to composite. 

</details>

<details>

     <summary>
     
     Processing
     
     </summary>

- Generally, we want to process the most representative sample possible.  Different people might think of this in different ways, but you should allow this to be your guide when choosing which parts or which pieces to process.  If there are a multitude of different colors, sizes, shapes, try to either get a variety that ranges between all of the variables, or choose ones that are the most common if they are the most prevalent variables. For example, if you have 15 blueberries, 5 small, 5 medium, 5 large, it might be best to choose one from each size category as they are all equally represented.  If you have 1 small, 13 medium, and 1 large, it would be best to choose all three from the medium size as that is the most representative. Use your best judgement.  
- Keep in mind that you might receive a bunch of samples that have likely been sourced from three separate locations in the field for compositing into one (if this applies to your lab!).  If you can see a pattern that makes it obvious which samples are from different areas, try to choose one from each area.  If it isn't obvious how they are distributed, try to choose the most representative samples.  We can only do our best with what we are given.  
- Always attempt to process the parts of the samples that are commonly consumed.  This is difficult as some people will eat the skin of a carrot and some don't.  We have done research to determine which parts of the produce are most commonly consumed and have tried to add instructions that will convey that.  However, you might find that your eating habits don't match what we've designated.  That's ok, but try to be consistent and follow the procedures that are set out here. i.e. Most people would not eat the roots or stems of a beet, so we cut those off for processing.  
- There are specific weight measurements that we take for each produce type.  If we enter the exact weight measurement, the software will calculate the concentration of sample based on that measurement.  However, we always try to take a measurement close to the specified volume as it will affect the wet chemistry dilution factors if you use a different amount and might lead to an unnecessary dilution or small concentration.  

| Fruits, roots, and tubes | Weight (g)| Leafy Greens | Weight (g) | 
| ------ | ------ | ------ | ------ |
| Apple | 1 | Bok Choy | 1 |
| Beets | 1 | Chard | 1 |
| Blueberry | 1 | Kale | 1 |
| Butternut | 5 | Leek | 2 |
| Carrot | 5 | Lettuce | 2 |
| Grapes | 1 |  Mizuna | 1 |
| Nectarine | 2 | Mustard | 1 |
| Peaches | 1 | Spinach | 1 |
| Pears | 2 |
| Potato | 4 | 
| Pepper | 1 |  
| Squash/Zucchini | 4 |  
| Tomato | 1 |  

</details>

<details>

     <summary>
     
     Juice
     
     </summary>

Juice samples are collected after all other measurements are taken and are done only if there is enough sample to produce at least 1mL of juice.  Most juice is produced using the garlic press after processing specific to the produce type such as grinding or grating.  If you have trouble getting juice out of the grated pieces, you can try grinding with the coffee grinder as an alternative.  You'll get the best results if you squeeze the garlic press firmly enough to start pressing out the juice, but not so firmly that the solids are pushed through the holes in the press.  You'll also have an easier time if you use surface tension to help pull the juice out by touching the surface of the press to the sieve.   Do not use the plant tissue left over after juicing for moisture content or wet chem as it will give you inaccurate results, it should always be discarded.  

For the reflectometer scan, you generally want to make sure that the volume of juice is above the  reflectometer.  680mL is usually a good volume to start with.  Some of the juices are very sticky and you will need to ensure that you are cleaning the cuvettes well.  

For the full spec, exactly 1mL is required.  It is often best to take the full spec scan first if there is limited amounts of juice as this is the minimum required, but the other two processes don't require as much and can be done with the same juice. Ensure that the surface of the glass dish is fully covered with juice and place it in the glass dish holder attachment and cover with the full spec lid.

Take a measurement of the juice with the brix meter to determine sugar content, ensuring to fully cover the optical window with the juice.  Clean out the juice out of the brix meter after each use.

</details>

## Instructions by Produce Type

The following sections explain the process specific for each produce type.

<details>

     <summary>
     
     Apple, Nectarine, Peach, Pear
     
     </summary>

### Scanning:

When scanning with the reflectometer, use the round fruit attachment. Follow the round fruit scanning instructions in the General Process Notes.  These produce types are very likely to have multiple colors, ensure you are taking a representative scan.  

### Density Measurement:

Usually all three pieces will fit into a graduate cylinder for volume measurements and onto a large balance for weight measurements.  Please review the density instructions for alternatives if needed.

### Sample Preparation:
Each individual apple is a subsample.

Conduct the following process on each piece that is being composited and then ensure a through homogenization of all samples before collecting them for the different tests.  

Cut two cheeks off of either side of the produce, leaving a strip the width of the microplane through the diameter of the apple. You may discard one cheek. The cheeks will be grated with the large grater into a bowl and used for moisture content and juicing. Make sure you grate it from the narrow side so that you are grating the skin and the flesh at the same time and collecting a representative sample. Do not start grating from the skin side or the flesh side as you will not get an accurate mixture of the two. Only one cheek from each apple needs to be processed. 

The middle strip will be grated from the skin all the way to the core using the microplane and this will be used for the wetchem sample only. Work fast to reduce oxidation of the sample. When grating the skin, make sure and use short, quick strokes to ensure that the skin is grated as finely as possible.

Be mindful of skin color. If there are multiple colors in the skin, try to use the most representative sample when determining which part of the apple to use for the wetchem test. Keep in mind that skin color as well as skin:flesh ratio have a big impact on nutrient content.

### Juice:
Juicing instructions

The cheek that was grated for use in moisture content is also used for juicing.  Following the general juicing instructions for use with the large grated pieces in the garlic press. 

[Video Link for Apple, Nectarine, Peach, Pear] (https://youtu.be/0NWJCu4gmy8)


</details>

<details>

     <summary>
     
     Beets, Carrots, Potato, Yellow Squash, and Zucchini
     
     </summary>

### Scanning:

When scanning with the reflectometer, use the round fruit attachment. Follow the round fruit scanning instructions in the General Process Notes.  Particularly with beets, carrots, and potatoes, ensure the skin is free from dirt.  Yellow squash and zucchini are treated as the same produce type for our purposes. 

### Density Measurement:

Usually all three samples will fit into a graduate cylinder for volume measurements and onto a large balance for weight measurements.  Please review the density instructions for alternative methods if needed.  

### Sample Preparation:

Each individual piece of produce is a subsample.

Peel beets and carrots before processing, all others are processed with skin on.  Scans and density measurements should be taken before peeling.

Remove any roots or stems from the produce that are not generally consumed. Take an equivalent, lengthwise slice from each of the subsamples (similar to an orange slice). The size of the slice is dependent on how much sample you need. As long as you use an equivalent amount from each subsample, size doesn't matter. Grate all three pieces with the large grater. Use the grated pieces to fill the moisture content cup, weigh out the appropriate amount of wet chem sample, and for juicing.  If you are only processing one piece of produce, just ensure you are taking a representative slice of the produce.  Generally cutting lengthwise from top to bottom and from skin side through to the core is our standard as it will contain all of the areas that have differing structures in the one piece of produce.  

### Juice:

Use the grated sample for juicing.  Following the general juicing instructions for use with the large grated pieces in the garlic press. 

[Video Link for Beets, Carrots, Potato, Squash, Zucchin](https://youtu.be/G73RaFIGqwo)

</details>

<details>

     <summary>
     
     Bok Choy and Head Lettuce
     
     </summary>

### Scanning:

Scan the leaves following the process in the General Process Notes Section for Scanning Leafy Greens. For Bok Choy, always scan the dark green portion of the leaves. 

### Leaf Length Measurement:

Scan the longest leaves from each head following the Leaf Length Measurement process in the General Process Notes Section

### Sample Preparation:

1 head of lettuce or bok choy = 1 subsample.  

Take at least one leaf from each subsample and homogenize in the coffee grinder. If necessary, add more leaves, just make sure you take an equivalent, representative sample from each subsample. For bok choy, it is helpful to thinly slice them first so that the white part grinds into small pieces more easily. Make sure you include all of the white part and the green part from each leaf. For lettuce, make sure you remove the ribs from the lettuce.  Once finely ground, weigh out the required amount of wetchem sample and use the leftovers for juicing. For moisture content, if you are using baby bok choy you may be able to roll up three leaves together to fit in the moisture content cup but if you are using large bok choy you will have to cut an equivalent sized strip down the length of each leaf and roll them up to fit into the cup. Make sure you get both the white and green part and at least one equivalent portion from each subsample. 

### Juice:

Use the homogenized mixture that was ground in the coffee grinder for juicing, following the juicing process mentioned in the General Process Notes. 

[Bok Choy and Head Lettuce] (https://youtu.be/Ct0LRs1IGCA)

</details>

<details>

     <summary>
     
     Butternut Squash
     
     </summary>

### Density Measurement:

Because we scan butternut samples after cutting the skin, we are required to take density measurements first.  If compositing up to three butternut samples, you will need a very large vessel for the volume measurements.  If necessary, you can do each piece individually, using the same initial volume and adding the change of volume for all three plus the initial volume to determine the final volume.  It is expected that the squash will be too large for most scales, so they are entered individually for butternut squash only.  

Calculate volume displacement for multiple butternut that are measured separately:

final volume butternut #1 - initial volume = displacement #1
final volume butternut #2 - initial volume = displacement #2
final volume butternut #3 - initial volume = displacement #3
Displacement #1 + Displacement #2 + Displacement #3 + Initial Volume = Combined final volume


### Scanning:

For scanning butternut we use a pumpkin punch to remove a sample of each squash using the following method.  Brush off any excess dirt before scanning. Punch out a piece of the butternut squash with a pumpkin punch.  Cut towards the top where it is more fleshy, avoiding the hollow center with seeds and stringy pulp. Make sure you take a sample large enough to fully cover the optical window of the reflectometer and full spec. Punch out three separate pieces and scan them, skin side down, on the optical window.  Watch for juice and clean the optics if necessary, between each measurement.

### Sample Preparation:

Using the punched out portions that you took from each of the squash subsamples in the scanning step, grate the pieces with the large grater, grating the whole piece but avoiding the skin. Make sure all three pieces are grated and mixed together well to create a representative sample of the three. Use this to fill the moisture content cup, measure out the appropriate amount for wet chem and for juicing. If you run low on sample, punch out additional pieces from each of the composited samples and repeat the process until you have enough sample for all the required tests.

### Juice:

Use the grated pieces for juicing.  If you find that you cannot get enough juice out of the samples you can either punch out additional pieces (make sure you take sample from each item being composited) or you can try grinding the samples in the coffee grinder to release more juice. 

[Video Link for Butternut Squash] (https://youtu.be/DqopiUb4EsU)

</details>

<details>

     <summary>
     
     Chard, Mustard, Kale, Spinach, Mizuna
     
     </summary>

### Scanning:

Follow instructions on scanning from the General Process Notes for Scanning Leafy Greens.

### Leaf Length Measurement:

Scan the longest leaves in each head or bunch, following the Leaf Length Measurement process in the General Process Notes Section

### Sample Preparation:

One bunch or head is considered a subsample.  Process 1-3 subsamples.

Choose at least one leaf from each subsample, or at least three representative leaves if the subsamples aren't obvious or if they were received as one big bunch. Use more if needed. For moisture content you will need to either use three leaves rolled up or cut out an equivalent strip lengthwise from each of the leaves and roll them up to put into the moisture content cup. The left over leaves (or additional leaves) can be ground up in the food processor. Once finely ground, weigh out the appropriate amount of sample for wet chem and use the rest for juicing.

### Juice:

Use the ground, homogenized sample for juicing. Kale can be particularly thick and difficult, you may need to use a large volume of sample to get the 1mL minimum volume. Remember to apply firm but gentle pressure to the garlic press.

[Video Link for Leafy Greens] (https://youtu.be/M7IyiOeL0jI)

</details>

<details>

     <summary>
     
     Grapes, Blueberries, Cherry Tomatoes
     
     </summary>

### Scanning:

Follow the general procedures for scanning round fruit in the General Process Notes.  For blueberries, they need to fully cover the optical window or you should not complete the scan.  If  you don't have any that are large enough, do not complete the scans and note the reason for skipping the scans in the notes section in the survey.  Always ensure that you are scanning a clean, unblemished surface, preferably on the side of the round fruit, ensuring to avoid any piths, roots, stems, leafy bits.  Grapes, blueberries, and cherry tomatoes all have dots or piths where they were connected to the stem, avoid those areas.  For these small fruits you will need to choose three representative pieces for this measurement, however, you will use more for the next parts of the process.

### Density Measurement:

Most blueberries, grapes, and cherry tomatoes are quite small and you will need to do your density measurements on a larger quantity than three.  This is ok, just make sure you take the weight and volume measurements with the exact same pieces, choosing the most representative samples.  The grapes, cherry tomatoes, and blueberries are often shipped in a small plastic cup., the entire contents of which can be used in the density measurement.  Please ensure you use more than just three pieces of these small fruits to conduct the density measurements.

### Sample Preparation:

Use all of the samples from the shipping cup that you used to take density measurements, to conduct this process. For grapes and cherry tomatoes, cut each piece into three sections, consisting of 1 half and two quarters. Make three separate piles so that each pile includes part of each piece of fruit. For example if you have 5 cherry tomatoes, you will have one pile with 5 of the 1/2 pieces, another pile with 5 of the 1/4 pieces, and a third pile with 5 of the 1/4 pieces. The pile of 1/2 pieces will go into the coffee grinder to be ground for wetchem and juicing samples. Make sure they are as finely ground as possible so that you do not get too much of one part of the fruit in the sample. The first pile of 1/4 pieces should be placed in the moisture content cup. The second pile should be used for juicing. For Blueberries, use whole fruit for moisture content. Grind the whole fruit in the coffee grinder for wetchem and juicing. Cutting individual sections out of these small berries is just not feasible.

### Juice:

Use the second pile of 1/4 pieces of grapes and cherry tomatoes that you separated out in the sample preparation steps for the juicing. Grapes will juice easily.  Cherry tomatoes are more difficult to juice because they tend to have a lot of fiber and seeds, so watch for that.  Blueberries are very tricky because they emulsify rather than just releasing juice.  We found the following method to produce the best results. Place a chlorine free paper towel into the garlic press so that you cover all sides. Fill with the ground blueberries and very gently squeeze out the juice into the sieving cup until you have at least 1 mL of sample. If you squeeze too firmly, the paper towel will break and the blueberry emulsion will go everywhere. Take care and do this slowly and gently for best results.

[Video Link for Grape, Blueberries, Cherry Tomato] (https://youtu.be/Ct0LRs1IGCA)

</details>

<details>

     <summary>
     
     Leeks
     
     </summary>

### Scanning:

Scan the dark green portion only of each stalk following the General Process Notes.  

### Leafy Lenth Measurements:

Using the whole stalk, measure from the roots to the tip of the leaf. 

### Sample Preparation:

Each complete leek stalk is a subsample.

Line the three subsamples up together so that the line that divides the white part from the green part are all at the same point. Cut off the roots and the leaves of each subsample, so that you are left with the middle part of the vegetable that is cylindrical in shape and has a gradient from white to green. Cut that cylindrical part in half vertically so you are left with two halves of each subsample that represent all of the colors in the gradient. On one half of each of the subsamples, cut out a pie shaped slice of each that will fit into the moisture content cup. With the second half of each, finely slice them and then grind them in the coffee grinder for use in wetchem and juicing.

### Juice:

Use the ground portion and follow the General Process Notes procedure.  Leeks are surprisingly good at producing juice.

[Video Link for Leeks] (https://youtu.be/GVBDEnBH_zM)

</details>



<details>

     <summary>
     
     Sweet Peppers
     
     </summary>

### Scanning:

Scan peppers following the procedure for scanning round fruit in the General Process Notes. Sweet peppers are particularly flexible and it's easy to cover the whole optical window by pressing down firmly.  

### Density Measurement:

Follow the procedure for taking density measurements in the General Process Notes.  

### Sample Preparation:

Each sweet pepper is a subsample.

Cut out the stem, core, and seeds from each of the subsamples. Cut out an equivalent portion of each of the subsamples that include the sides, top, and bottom of the sample (essentially a slice of the pepper). Using the large grater, grate all three equivalent sized subsample pieces. Mix the pieces from all subsamples well and use these for wetchem, moisture content and juicing.

### Juice:

After collecting enough samples for wetchem and moisture content, pour the juice that has collected in the bowl through the sieving cup. If there is not enough juice, place the large grated pieces into the garlic press and squeeze into the sieving cup, until you have at least 1mL of sample.

[Video Link for Peppers] (https://youtu.be/t-sCyRg25T0)

</details>

<details>

     <summary>
     
     Tomatoes
     
     </summary>

### Scanning:
 
Scan tomatoes following the procedure for scanning round fruit in the General Process Notes.

### Density Measurement:

Follow the procedure for taking density measurements in the General Process Notes.  Take extra care not to damage the tomato or allow breakage before taking the volume measurement as introducing water into the tomato may result in inaccurate data for other measurements such as moisture content. 

### Sample Preparation:

Each individual tomato is a subsample.

Take an equivalent, vertical slice out of each subsample and place in the moisture content cup. Take an additional slice out of each subsample and grate with the large grater. Use this for wetchem and juicing. Be sure to thoroughly mix all three grated subsamples. This releases a lot of juice so an spoon might be helpful to mix. Make sure you are not draining all of the juice out of your wetchem sample before collecting it.

### Juice:

After collecting enough samples for wetchem and moisture content, pour the juice that has collected in the bowl through the sieving cup. If there is not enough juice, place the large grated pieces into the garlic press and squeeze into the sieving cup, until you have at least 1mL of sample.

[Video Links for Tomatoes](https://youtu.be/AfGX7IUZHQk)

</details>

## Grains Intake

Grains use the sample survey as produce for intake "2021 RFC Intake Survey".  Much of the same processes and equipment are used for grains and you should read the previous sections regarding the survey and General Process Notes for guidance.  Any specific differences for processing grains are set out in the instructions below. The main differences are that there are no density, length, or juice measurements required for grains and we use both whole grains and ground flour. 

If grains are not dehulled when they arrive to the lab, use a rice dehuller or a mortar and pestle to remove the hulls from as many grains as you will need to processing samples.  This can be a very time consuming process, so only prepare what you need.   Do not process samples with hulls still attached.

### Scanning:

Grains require scans for both whole grains and flour.

Surface Scans 1, 2, 3: Whole Grains

homogenize a representative sample from up to three grain samples by mixing together thoroughly in a dish or bag. Place the whole grain into a glass dish and place over the optical window (using the catch on the cuvette holder to keep it out of your way). Scan three times.  Pour the grains into the bag and then back into the glass dish in between measurements to ensure thorough mixing and more accurate results.

Ground Scan 1, 2, 3: grain flour

Place the whole grain sample that has already been scanned into the coffee grinder and grind till fine. Pass through a 2mm sieve into a plastic bag to ensure you have a finely ground powder. Fill a glass dish with the flour sample. Scan three times, Pour the ground flour into the plastic bag and then back into the glass dish in between measurements to ensure thorough mixing and more accurate results. After scanning, the flour will be used for wetchem.

You can prepare twice as much whole grain as you need if you'd like to scan half the whole grain on the full spec while you are grinding the other half into flour.

### Sample Preparation:

Use whole grain samples to add to the moisture content cup. Use the flour that you prepared during the scanning step to measure out the appropriate amount of sample for wetchem.  Weigh out the ground sample into 50mL tubes for grains only. 

[Video Link for Grains] (https://youtu.be/tT9y3Cm25cc)

