# Link to Lab Equipment

[Lab Equipment](https://docs.google.com/spreadsheets/d/13hsFXMrDMSm6wZBeLG9cz_OGs9uorBCjUjsCay8TqmI/edit#gid=0)

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

