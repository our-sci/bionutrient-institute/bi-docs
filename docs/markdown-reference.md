
Markdown How-To:

This is the best place to start with any questions:
![Markdown Guide](https://www.markdownguide.org/cheat-sheet/)

**bold**
*italics*
<em>highlight</em>
# Heading 1 (largest heading size, can use up to four #)
## Heading 2 

- bullet 1  
- bullet 2

link structure:
[Food Access Research Atlas](2018_report.md)

images:
--> make sure to right click and hit copy image address
![image](https://gitlab.com/our-sci/bionutrient-institute/bi-docs/-/raw/master/docs/img/cat-professional.jpeg)


table
| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |




...
