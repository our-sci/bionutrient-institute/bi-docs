# Moisture Content and XRF Prep

### Moisture Content Software:
**Dry Weight Software:** 2020 Produce Dry Weights

After samples are dried in the food dehydrator for more than 24 hours, they will be weighed in batches of 24 (the number that can be held in the mini-muffin tins). This allows us to calculate moisture content. The calculation will be done in a separate survey.

1. Indicate whether the tablet is connected to the balance or not.

2. Enter Lab ID

3. Enter Dry Weight

4. Repeat for all 24 samples. If there are less than 24 samples that is ok. 

5. Enter any notes about this batch that may be relevant. Include Lab IDs as necessary.

6. Hit Send
