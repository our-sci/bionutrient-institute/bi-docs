# BI Partner Lab Memorandum of Understanding

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

## 1) Purpose

This document ('MOU') provides a framework and sets expectations for collaboration between the Bionutrient Institute (formerly the Real Food Campaign) project of the Bionutrient Food Association (BFA), the BI Main Lab, and BI Partner Labs in service to our **Shared Mission**:

> ... surveying the quality of soil and food samples and making a large, public, high quality and well referenced dataset.

See the [Bionutrient Institute website](https://www.bionutrientinstitute.org/) for detailed mission and goals.

The BI, BI Main Lab and BI Partner Labs genuinely believe in and are committed to supporting this shared mission.

The details of this MOU advances our _Shared Mission_ by:

1. expanding the geographic impact of the survey.
2. increasing the number of samples while decreasing per-sample cost
3. diversifying expertise to more quickly improve lab and field methods.
4. increasing impact through greater public awareness and trust
5. diversifying motivations to expand project outputs (publications, certifications, etc.)

## 2) Definitions

**BI Main Lab**: The main BI Lab, appointed by the BFA, located in Ann Arbor, Michigan.

**BI Main Lab Director**: The director of the _BI Main Lab_, appointed by the BFA.  

**BI Partner Labs**: Any lab who has signed and abides by this MOU.

**BI Partner Lab Zone**: The geographic area in which the _BI Main Lab_ will recommend samples be sent to an individual _BI Partner Lab_.  

**BI Lab Process**: The most up-to-date set of methods and Standard Operating Procedures (SOPs) approved by the _BI Main Lab_ for processing soil and food samples.

**BI Samples**: Samples processed using the _BI Lab Process_ by a _BI Partner Lab_ (either paid or unpaid by the sender)

**BI Survey Samples**: _BI Samples_ for which 1) the processing _BI Partner Lab_ has NOT been paid by the entity providing the sample, and 2) the sample type fits into the existing _Yearly Lab Plan_.

**Paid Samples**: _BI Samples_ for which the processing _BI Partner Lab_ has been paid by the entity providing the sample.  

**Cross-check Program**: A program to compare the outputs of _BI Main and Partner Labs_ to quantify comparability, accuracy, and to identify points of improvement with the process.

**Additional Documents**: Other agreements listed below and referenced in the MOU.

These documents are 'living' and constantly updated:

* **[BI Lab Process](https://gitlab.com/our-sci/bionutrient-institute/bi-docs/-/blob/master/docs/Lab%20Processes%20TOC.md)**: 

     * Full description of the equipment, standard operating procedures, and information systems used to collect an _BI Sample_.
* **[BI Lab Communication Channels List](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/lab_communication_channels/)**:

     * Communication on slack / riot (general, change request / approval).
     * Meeting dates (web, in person if possible!), locations, standard agenda... etc.
* **[New Lab Checklist](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/new_lab_checklist/)**:

     * Onboarding checklist for new _BI Partner Labs_.

These documents should be signed independently of this MOU.  If they are changed, parties should should resign the new version.

* **BI Code of Conduct**: Coming soon (signed by organizational representative)
* **BI Privacy Agreement**: (signed by all individuals)

     * No sharing anonymized information.
     * Any release of anonymized information must be approved by and come directly from the _BI Main Lab Director_.   

## 3) Organizational Chart, BI Lab Network

![BI_Lab_Network_Outline](https://gitlab.com/our-sci/bionutrient-institute/bi-docs/-/raw/master/docs/img/BI_Lab_Network.png)

## 4) What the _BI Main Lab_ provides

To support _BI Partner Labs_, the _BI Main_ Lab will:

`[Insert Agreement Details here]`

## 5) What the _BI Partner Lab_ provides

To support our _Shared Mission_, the _BI Partner Lab_ will:

`[Insert Agreement Details here]`

## 6) Additional Legal Stuff

1. Term of MOU.  This MOU is effective upon the day and date last signed and executed by the duly authorized representatives of the parties to this MOU and the governing bodies of the parties’ respective counties or municipalities and shall remain in full force indefinitely.  This MOU may be terminated, without cause, by either party upon 30 days written notice, which notice shall be delivered by hand, by certified mail to the address listed above or by email to lab@bionutrient.org.  Parties shall review the document once per year, in conjunction with the creation of a new _Yearly Lab Plan_.

2. Amendments.	Either party may request changes to this MOU.  Any changes, modifications, revisions or amendments to this MOU which are mutually agreed upon by and between the parties to this MOU shall be incorporated by written instrument, and effective when executed and signed by all parties to this MOU.

3. Applicable Law.	The construction, interpretation and enforcement of this MOU shall be governed by the laws of the State of Michigan, USA.  The courts of the State of Massachusetts shall have jurisdiction over any action arising out of this MOU and over the parties, and the venue shall be the `[Insert County]` County, Massachusetts, USA.

4. Entirety of Agreement.  This MOU represents the entire and integrated agreement between the parties and supersedes all prior negotiations, representations and agreements, whether written or oral.

5. Severability.	Should any portion of this MOU be judicially determined to be illegal or unenforceable, the remainder of the MOU shall continue in full force and effect, and either party may renegotiate the terms affected by the severance.

6. Third Party Beneficiary Rights.  The parties do not intend to create in any other individual or entity the status of a third party beneficiary, and this MOU shall not be construed so as to create such status.  The rights, duties and obligations contained in this MOU shall operate only between the parties to this MOU, and shall inure solely to the benefit of the parties to this MOU.  The provisions of this MOU are intended only to assist the parties in determining and performing their obligations under this MOU.  The parties to this MOU intend and expressly agree that only parties signatory to this MOU shall have any legal or equitable right to seek to enforce this MOU, to seek any remedy arising out of a party’s performance or failure to perform any term or condition of this MOU, or to bring an action for the breach of this MOU.

7. Signatures.  In witness whereof, the parties to this MOU through their duly authorized representatives have executed this MOU on the days and dates set out below, and certify that they have read, understood, and agreed to the terms and conditions of this MOU as set forth herein.

`[Fill in partner lab name here]`

_______________
_name_, _signature_, _date_

Partner Lab Director


_______________
_name_, _signature_, _date_

Executive Director, Bionutrient Food Association

