# New Lab Checklist

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

This checklist is to support new labs joining the RFC Lab Network.  This assumes that you will be sampling by June 1st.  If the sampling start date is different, adjust the dates appropriately.

### By January 1
* [ ] Read background information about the RFC and RFC Lab Partners
	* [About the RFC](https://lab.realfoodcampaign.org/about/)
	* [RFC Partner Lab Memorandum of Understanding](RFC-Partner-Lab-Memorandum-of-Understanding)
	* [RFC Lab Process + Equipment List](Lab-Process)
        * [Equipment List](https://docs.google.com/spreadsheets/d/13hsFXMrDMSm6wZBeLG9cz_OGs9uorBCjUjsCay8TqmI/edit?usp=sharing)
* [ ] Have a discussion with the ED of the Bionutrient Food Association and Main Lab Director to go through details.  Specifically, discuss:
	* Capacity + costs (space, equipment, training/setup, personnel, skills, supplies)
	* Crop quantities + types (do we have a process for your crops of interest?  How much can you run, etc.)
	* Sample collection support (connections to local farmer groups, supply chains, groceries, consumer groups, etc.)
	* Fit with existing goals (goals of your lab, goals of RFC, overlapping / conflicting interests, long term revenue models, etc.)
	* Reimbursement (can you cover labor + other non-reimbursed expenses?)
	* Marketing, branding, fundraising opportunities
	* Other research collaboration opportunities
* [ ] Decide if this feels like a good fit... if so...

### By end of January
* [ ] Have lab personnel + admin's join the appropriate RFC Communication channels for effective discussion and support.
* [ ] Meet with RFC Main Lab Director to:
	* Review the RFC Lab Process + Equipment List)[RFC-Lab-Process] - discuss questions, concerns, etc. with RFC Main Lab.
	* Review the RFC Yearly Plan, create targets and goals for this year.  This will generate supplies, labor, and other requirements.  Discuss with Main Lab for planning purposes. 
* [ ] Identify equipment you need and **order long lead time items!!!**
* [ ] Set up date for Main Lab to visit (gives Main Lab time to prepare)

### By end of March
* [ ] Receive + setup equipment
* [ ] Prepare lab space, ensure that people + hours are available.
* [ ] Have visit from Main Lab
* [ ] Have a clear understanding of sample sourcing for the coming year
	* (outside US) - Work with local farmer groups, grocery stores, consumer organizations to organize samples to be sent to your lab.  If you are doing this, make sure to think about things like:
	* (inside US) - Communicate with the RFC Data and Farm Partner Manager to ensure they know which samples from which are will be sent to you in the coming year.  If you have additional farmers or sampling locations you want to manager, communicate that to the RFC Data and Farm Partner Manager as well just so they are aware.
	* Shipping costs, times, box / bag type, etc.
	* RFC Sample Collection forms using the RFC Collect App - are they in the correct language?  Are the questions / options appropriate for your data / farm partners?  Do partners have compatible devices?


### By end of April
* [ ] Completed dry runs for most of the RFC lab methods

### By end of May
* [ ] Complete dry runs for all of the RFC lab methods
* [ ] Complete training + test sample submissions from Farm and Data Partners or other local collection sources.
* [ ] Plan for or complete an cross-check with other labs (targeted to happen in May / June).

### June 1 - begin accepting samples!