# Soil Respiration

Respiration data will be recorded in the following two separate surveys, **2020 RFC Baseline Respiration** and **2020 RFC Final Respiration**. The survey will record data by soil batches of 36, consisting of one QC standard and 35 samples.

### Required Equipment

- CO2 Meter
- Syringe w/ luer lock stopcock
- 1 mL Pipetter
- Small Aluminum Tins
- DI Water
- Thermometer

TODO: Add pictures of each piece of equipment

1. Open the **2020 RFC Baseline Respiration** survey. These are the general instructions as to how to conduct respiration. It is important to also record the temperature during the 24 hr period using a thermometer that can record the min, max, and average temperatures. 

2. Record the Tray ID for this batch of soils.  

3. Record the LAB ID#.  

4. Insert the tip of the syringe into the CO2 meter.

5. Ensure the CO2 meter is connected through Bluetooth. The meter must be connected to a power source. Connect to the device via Bluetooth through the Our Scikit app by using the device symbol in the upper right. If there is a lag between initialization and measurement, the sensor may time out and the first measurement may return an error. If it takes longer than 90 secs to return a reading, you will need to stop the measurement and reset the position of plunger in the syringe.

6. Using Our Scikit app, press "Start Measurement" for time zero.

7. Wait for 7-10 seconds after starting measurement, and as rapidly as possible, push plunger to the 90 mL mark and wait for measurement to complete.

8. Once the CO2 curve appears on screen, turn the stopcock on the syringe to close the opening and set the syringe aside carefully. Make sure to not spill the soil within the syringe. On the first syringe, record the time you started the measurement.

9. Complete this step for all samples in the batch. Ensure you hit ‘Send’ on the final page to ensure the information goes to the database.

10. Wait 24 hrs from the time on the first syringe before completing the next step.

11. Open the next survey, **2020 RFC Final Respiration**.

12. Enter the Tray ID for this soil batch.

13. Record the Average temperature during the 24 hr incubation period.

14. Record the MAX temperature during the 24 hr incubation period.

15. Record the MIN temperature during the 24 hr incubation period.

16. Record the LAB ID for sample #1.

17. Insert syringe back into the CO2 injection port.

18. Open stopcock and take the final CO2 reading by pressing "Start Measurement" in the software.

19. Wait for 7-10 seconds after starting measurement and then rapidly push plunger to the 60 mL mark and wait for the end of measurement. 

20. Once all 36 samples have been tested for the final respiration, press the "Calculate Soil Respiration" button to see the results. 

### Common Survey Errors

1. Extremely high values: If the result of a measurement is very high (>10,000) but everything else looks normal, the test should be run again. If at time zero, pull the plunger out and start again. If at time 24 hrs, do not remove the plunger, just push it forward again as far as you can and take a new measurement. 

TODO: Add example image

2. Smoothing error message: Due to the nature of the sensor, it will occasionally capture a spike in results that isn’t consistent with the linearity of the measurement. The software will smooth these spikes and you will receive a yellow message.  This is nothing to worry about and you may proceed as normal. However, if you get this message consistently, one after another, it is worth contacting Our Sci, LLC to investigate the problem.

3. Values too high error: Very rarely, you will receive an error message at the 24 hr mark that the values are too high. If this occurs, you should rerun the sample to verify that the sample is in fact above the instruments detection level. If you get a second high result, you will need to mark this as higher than detectable measurement with the device. At this time we do not have a method to determine the true result for these samples.

4. If anything looks unusual, please investigate. Don’t hesitate to contact Our Sci, LLC with any questions.  