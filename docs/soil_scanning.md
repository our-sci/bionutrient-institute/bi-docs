# Soil Scanning

### Required Equipment

The survey you will need for this SOP is **2020 RFC Soil Scans**- this survey includes the steps for reflectance scan as well as creating Lab ID#s.

- Reflectometer
- Glass Dish  

1. Enter the ID number that came with the sample.

2. Enter the internal Lab ID that will be used to track the sample through the testing process.

3. Indicate the depth of soil the sample was taken from.

4. Indicate if the soil is of normal or high organic matter. This information is important as it will tell us how to handle this soil during pH testing and if we are likely to see higher than normal values of total carbon during LOI. A sticker will be applied to the sample bag so that it is clear to the lab technician how to handle the sample.

5. Next estimate whether or not we have enough sample to complete all testing. This also helps to determine how to prioritize testing based on what is available.

6. Pour the soil into a small, round, glass dish, making sure there is enough soil in the dish to fully cover the bottom. Place the dish onto the reflectometer so that it completely covers the optical window and press "Take Measurement".  

7. Turn the dish ⅓ and take a second reading.

8. Tray ID# (batch #) This is where the Tray ID will first be linked to each sample in the batch.

9. Record any important notes about the sample before pressing "Send".